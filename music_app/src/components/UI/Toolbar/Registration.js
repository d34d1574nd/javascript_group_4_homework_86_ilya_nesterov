import React, {Component, Fragment} from 'react';
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

class Registration extends Component {
  render() {
    return (
      <Fragment>
        <NavItem>
          <NavLink tag={RouterNavLink}  to="/registration" exact>Sign Up</NavLink>
        </NavItem>
        <NavItem>
          <NavLink tag={RouterNavLink} to="/login" exact>Login</NavLink>
        </NavItem>
      </Fragment>
    );
  }
}

export default Registration;