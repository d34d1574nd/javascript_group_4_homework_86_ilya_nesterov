import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink} from "react-router-dom";

const Login = props => {
  return (
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret>
        Hello, {props.user.username}
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem>
          <NavLink to='/track_history'>Track history</NavLink>
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem>
          Log out
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
};

export default Login;