import React from 'react';
import {Nav, Navbar, NavbarBrand} from "reactstrap";
import Login from "./Login";
import Registration from "./Registration";
import {NavLink as RouterNavLink} from "react-router-dom";

const Toolbar = ({user}) => {
  return (
    <Navbar  color="light" light expand="md">
      <NavbarBrand tag={RouterNavLink} to="/">Music app</NavbarBrand>
      <Nav className="ml-auto" navbar>
        {user ? <Login user={user}/> :  <Registration/> }
      </Nav>
    </Navbar>
  );
};

export default Toolbar;