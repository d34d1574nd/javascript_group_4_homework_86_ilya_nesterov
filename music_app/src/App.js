import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Switch, Route, withRouter} from "react-router-dom";
import {connect} from "react-redux";

import ArtistPage from "./containers/ArtsitsPage/ArtistPage";
import AlbumPage from "./containers/AlbumPage/AlbumPage";
import TrackPage from "./containers/TrackPage/TrackPage";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Login from "./containers/Login/Login";
import Registration from "./containers/Register/Register"

import './App.css';
import TrackHistory from "./containers/TrackHistory/TrackHistory";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar user={this.props.user}/>
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={ArtistPage}/>
            <Route path="/registration" exact component={Registration}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/albums/:id/:artist" exact component={AlbumPage}/>
            <Route path="/tracks/:id/:album/:artist" exact component={TrackPage}/>
            <Route path="/track_history" exact component={TrackHistory}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

export default withRouter(connect(mapStateToProps)(App));
