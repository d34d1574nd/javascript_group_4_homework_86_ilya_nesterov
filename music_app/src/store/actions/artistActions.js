import axios from '../../axios-api';

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';

const fetchArtistSuccess = artist => ({type: FETCH_ARTISTS_SUCCESS, artist});

export const fetchArtist = () => {
  return dispatch => {
    return axios.get('/artist').then(
      response => dispatch(fetchArtistSuccess(response.data))
    );
  };
};
