const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema ({
  titleAlbum: {type: String, required: true},
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true
  },
  year: {type: String, required: true},
  photoAlbum: {type: String, required: true}
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;