const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');
const config = require("../config");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathAlbum);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
  if (req.query.artist) {
    Album.find({artist: req.query.artist}).sort({year: 1}).populate('artist', '_id, name')
      .then(albums => res.send(albums))
      .catch(() => res.sendStatus(500))
  } else {
    Album.find().populate('artist', '_id, name')
      .then(albums => res.send(albums))
      .catch(() => res.sendStatus(500))
  }
});

router.post('/', upload.single('image'), (req, res) => {
  const albumInfo = req.body;

  if (req.file) {
    albumInfo.image = req.file.filename;
  }

  const album = new Album(albumInfo);

  album.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});

router.get('/:id', (req, res) => {
  Album.findById(req.params.id).populate('artist', '_id, name')
    .then(album => {
      if (album) res.send(album);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

module.exports = router;