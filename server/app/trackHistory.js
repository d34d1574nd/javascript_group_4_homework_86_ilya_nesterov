const express = require('express');

const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth, (req, res) => {
  const track = ({user: req.user._id, track: req.body.track, datetime: new Date().toISOString()});
  const trackHistory = new TrackHistory(track);

  trackHistory.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));

  res.send({Authorized: "Authorization was successful"})
});

router.get('/', auth, (req, res) => {
  TrackHistory.find({user: req.user._id}).populate({path: 'track', populate: {path: 'album', populate: {path: 'artist'}}})
    .then(track => res.send(track))
    .catch(() => res.sendStatus(500))
});

module.exports = router;