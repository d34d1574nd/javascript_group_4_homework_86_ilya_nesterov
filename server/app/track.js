const express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req, res) => {
  if (req.query.album) {
    Track.find({album: req.query.album}, null, {sort: {numberSong: 1}}).populate('album', 'titleAlbum')
      .then(track => res.send(track))
      .catch(() => res.sendStatus(500))
  } else {
    Track.find().populate('album', 'titleAlbum')
      .then(track => res.send(track))
      .catch(() => res.sendStatus(500))
  }
});

module.exports = router;